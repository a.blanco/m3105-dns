#!/usr/bin/env bash

## configuration de wiki.org 
# configuration du serveur dns
himage dwikiorg mkdir -p /etc/named
hcp Config_Machine/dwikiorg/named.conf dwikiorg:/etc/.
hcp Config_Machine/dwikiorg/* dwikiorg:/etc/named/.
himage dwikiorg rm /etc/named/named.conf 

## configuration de com.za 
# configuration du serveur dns
himage dcomza mkdir -p /etc/named
hcp Config_Machine/dcomza/named.conf dcomza:/etc/.
hcp Config_Machine/dcomza/* dcomza:/etc/named/.
himage dcomza rm /etc/named/named.conf 

## configuration de facebook.com.za 
# configuration du serveur dns
himage dfacebookcomza mkdir -p /etc/named
hcp Config_Machine/dfacebookcomza/named.conf dfacebookcomza:/etc/.
hcp Config_Machine/dfacebookcomza/* dfacebookcomza:/etc/named/.
himage dfacebookcomza rm /etc/named/named.conf 

## configuration de edu.za 
# configuration du serveur dns
himage deduza mkdir -p /etc/named
hcp Config_Machine/deduza/named.conf deduza:/etc/.
hcp Config_Machine/deduza/* deduza:/etc/named/.
himage deduza rm /etc/named/named.conf 

## configuration de iut.re 
# configuration du serveur dns
himage diutre mkdir -p /etc/named
hcp Config_Machine/diutre/named.conf diutre:/etc/.
hcp Config_Machine/diutre/* diutre:/etc/named/.
himage diutre rm /etc/named/named.conf 

## configuration de rt.iut.re 
# configuration du serveur dns
himage drtiutre mkdir -p /etc/named
hcp Config_Machine/drtiutre/named.conf drtiutre:/etc/.
hcp Config_Machine/drtiutre/* drtiutre:/etc/named/.
himage drtiutre rm /etc/named/named.conf 

## configuration de .org
# configuration du serveur dorg
himage dorg mkdir -p /etc/named
hcp Config_Machine/dorg/named.conf dorg:/etc/.
hcp Config_Machine/dorg/* dorg:/etc/named/.
himage dorg rm /etc/named/named.conf

## configuration de .za
# configuration du serveur dza
himage dza mkdir -p /etc/named
hcp Config_Machine/dza/named.conf dza:/etc/.
hcp Config_Machine/dza/* dza:/etc/named/.
himage dza rm /etc/named/named.conf

## configuration de .re
# configuration du serveur dre
himage dre mkdir -p /etc/named
hcp Config_Machine/dre/named.conf dre:/etc/.
hcp Config_Machine/dre/* dre:/etc/named/.
himage dre rm /etc/named/named.conf

## configuration de aRootServer
# configuration du serveur root
himage aRootServer mkdir -p /etc/named
hcp Config_Machine/arootserver/named.conf aRootServer:/etc/.
hcp Config_Machine/arootserver/* aRootServer:/etc/named/.
himage aRootServer rm /etc/named/named.conf

## configuration de bRootServer
# configuration du serveur root
himage bRootServer mkdir -p /etc/named
hcp Config_Machine/brootserver/named.conf bRootServer:/etc/.
hcp Config_Machine/brootserver/* bRootServer:/etc/named/.
himage bRootServer rm /etc/named/named.conf

## configuration de cRootServer
# configuration du serveur root
himage cRootServer mkdir -p /etc/named
hcp Config_Machine/crootserver/named.conf cRootServer:/etc/.
hcp Config_Machine/crootserver/* cRootServer:/etc/named/.
himage cRootServer rm /etc/named/named.conf

## configuration de pc1
# resolv.conf
hcp Config_Machine/pc1/resolv.conf pc1:/etc/.

## configuration de pc2
# resolv.conf
hcp Config_Machine/pc2/resolv.conf pc2:/etc/.

## configuration de www
# resolv.conf www de iut
hcp Config_Machine/www-iut/resolv.conf www-iut:/etc/.

# resolv.conf www de rtiut
hcp Config_Machine/www-rtiut/resolv.conf www-rtiut:/etc/.

# resolv.conf www de wiki
hcp Config_Machine/www-wiki/resolv.conf www-wiki:/etc/.

# resolv.conf www de eduza
hcp Config_Machine/www-eduza/resolv.conf www-eduza:/etc/.

# resolv.conf www de facebook
hcp Config_Machine/www-facebook/resolv.conf www-facebook:/etc/.

bash launch_dns.sh
